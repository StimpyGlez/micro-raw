package com.dgc.raw.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "CAT_RAW_INFORMATION")
public class JobConfiguration extends BaseEntity {

    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_JOB")
    private Long idJob;

    @Column(name = "ID_PALNTA")
    private String idPlanta;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "JOB_CRON")
    private String jobCron;

    @Column(name = "JOB_DEFAULT")
    private boolean jobDefault;

    @Column(name = "EXECUTE_JOB")
    private boolean executeJob;

    @Column(name = "EXECUTE_JOB_NO_DEFAULT")
    private boolean executeJobNoDefault;

}
