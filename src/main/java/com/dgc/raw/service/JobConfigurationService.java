package com.dgc.raw.service;

import com.dgc.raw.dto.SchedulerDTO;
import com.dgc.raw.Trigger.CronTigger;
import com.dgc.raw.factory.TriggerFactory;
import com.dgc.raw.domain.JobConfiguration;
import com.dgc.raw.repository.JobConfigurationRepository;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class JobConfigurationService {

    @Autowired
    JobConfigurationRepository repository;

    public void createJobs() throws Exception {

        log.info(" ===> CREACIÓN DE JOBS <=== ");

        List<JobConfiguration> jobConfigurations = getJobConfigurations();

        Scheduler scheduler = new StdSchedulerFactory().getScheduler();
        scheduler.start();
        for (JobConfiguration jobConfiguration : jobConfigurations){
            if (jobConfiguration.isExecuteJob()) {
                if (jobConfiguration.isJobDefault() || jobConfiguration.isExecuteJobNoDefault()) {
                    CronTigger cronTigger = TriggerFactory.getTrigger(jobConfiguration);
                    SchedulerDTO schedulerDTO = cronTigger.createJob(jobConfiguration);
                    scheduler.scheduleJob(schedulerDTO.getJobDetail(), schedulerDTO.getTrigger());
                }
            }
        }

        if (scheduler.getJobGroupNames().size() == 0 ){
            log.info("=== Nothing JOBS to do" );
        }

        log.info(jobConfigurations.toString());

    }

    private List<JobConfiguration> getJobConfigurations(){
        return repository.findAll();
    }

}
