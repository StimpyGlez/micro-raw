package com.dgc.raw;

import com.dgc.raw.service.JobConfigurationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@Slf4j
@SpringBootApplication
public class MicroRawInformationApplication {

	@Autowired
	JobConfigurationService service;

	public static void main(String[] args) {
		SpringApplication.run(MicroRawInformationApplication.class, args);
	}


	@PostConstruct
	private void init() {

	    try {
            service.createJobs();
        }catch (Exception e){
	        e.printStackTrace();
	        log.error(" === Error al generar Schedulers");
        }
	}


}
