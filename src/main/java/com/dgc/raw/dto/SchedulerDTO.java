package com.dgc.raw.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.quartz.JobDetail;
import org.quartz.Trigger;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class SchedulerDTO implements Serializable {

    private JobDetail jobDetail;
    private Trigger trigger;

}
