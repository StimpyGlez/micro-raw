package com.dgc.raw.Trigger;

import com.dgc.raw.dto.SchedulerDTO;
import com.dgc.raw.domain.JobConfiguration;

public interface CronTigger {

    public SchedulerDTO createJob(JobConfiguration jobConfiguration);

}
