package com.dgc.raw.Trigger.impl;

import com.dgc.raw.Trigger.CronTigger;
import com.dgc.raw.dto.SchedulerDTO;
import com.dgc.raw.domain.JobConfiguration;
import com.dgc.raw.jobs.PIJob;
import org.quartz.*;

public class PITrigger implements CronTigger {

    @Override
    public SchedulerDTO createJob(JobConfiguration jobConfiguration){
        JobDetail cetumJob = JobBuilder.newJob(PIJob.class).withIdentity(jobConfiguration.getDescription()+"_JOB", jobConfiguration.getIdPlanta()).build();
        Trigger cetumTrigger = TriggerBuilder.newTrigger().
                withIdentity(jobConfiguration.getDescription()+"_TRIGGER", jobConfiguration.getIdPlanta())
                .withSchedule( CronScheduleBuilder.cronSchedule(jobConfiguration.getJobCron())).build();
        SchedulerDTO schedulerDTO = new SchedulerDTO(cetumJob, cetumTrigger);
        return schedulerDTO;
    }

}
