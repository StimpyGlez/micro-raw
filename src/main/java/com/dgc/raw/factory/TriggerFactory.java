package com.dgc.raw.factory;

import com.dgc.raw.Trigger.*;
import com.dgc.raw.Trigger.impl.CetumTrigger;
import com.dgc.raw.Trigger.impl.DataBaseTrigger;
import com.dgc.raw.Trigger.impl.FileZilaTrigger;
import com.dgc.raw.Trigger.impl.PITrigger;
import com.dgc.raw.domain.JobConfiguration;

public class TriggerFactory {

    private final static String CETUM       = "CETUM";
    private final static String FILE_ZILA   = "FILE_ZILA";
    private final static String DATA_BASE   = "DATA_BASE";
    private final static String PI          = "PI";

    public static CronTigger getTrigger(JobConfiguration jobConfiguration){

        switch (jobConfiguration.getDescription()){
            case CETUM:
                return new CetumTrigger();
            case DATA_BASE:
                return new DataBaseTrigger();
            case FILE_ZILA:
                return new FileZilaTrigger();
            case PI:
                return new PITrigger();
            default:
                throw new RuntimeException("Unsupported Trigger type");
        }
    }
}
