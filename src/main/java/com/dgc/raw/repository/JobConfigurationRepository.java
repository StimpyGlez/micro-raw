package com.dgc.raw.repository;

import com.dgc.raw.domain.JobConfiguration;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobConfigurationRepository extends JpaRepository <JobConfiguration, Long> {

}
